export const App = () => {
  return (
    <div className='container main__page'>
      <h1>Use Hooks Typescript</h1>
      <p>
        Está es una aplicación con una librería de hooks de React, lista para usar, escrita en
        typescript.
      </p>
    </div>
  );
};
